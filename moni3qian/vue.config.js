module.exports={
  devServer:{
    port:8888,
    proxy:{
      '/api':{
        target:'http://localhost:8080',
        changeOrigin:'true',
        pathRewrite: {
          '^/api':''
        }
      }
    }
  },
  lintOnSave: false
}
