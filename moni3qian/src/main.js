import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios'
import moment from 'moment'

Vue.config.productionTip = false

axios.defaults.baseURL='/api'

Vue.prototype.$http=axios

Vue.filter('dateformat', function(dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)

})

Vue.use(ElementUI);

axios.interceptors.request.use(config=>{
  let tokens=sessionStorage.getItem("token")
  console.log(tokens)
  if(tokens!=null){
    config.headers.token=tokens
  }
  return config;
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')


