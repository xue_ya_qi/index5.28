package com.bw.entity;

import java.util.List;

public class Doubles {

    private Resume resume;

    private List<Experience> experiences;

    public Resume getResume() {
        return resume;
    }

    public void setResume(Resume resume) {
        this.resume = resume;
    }

    public List<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<Experience> experiences) {
        this.experiences = experiences;
    }

    @Override
    public String toString() {
        return "Doubles{" +
                "resume=" + resume +
                ", experiences=" + experiences +
                '}';
    }
}
