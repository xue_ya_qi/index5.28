package com.bw.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "checked")
public class Check implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "cid", type = IdType.AUTO)
    private Integer cid;

    private Date cdate;

    private String detail;

    private Integer rid;

    private Integer status;

    private Integer uid;

    @Override
    public String toString() {
        return "Check{" +
                "cid=" + cid +
                ", cdate=" + cdate +
                ", detail='" + detail + '\'' +
                ", rid=" + rid +
                ", status=" + status +
                ", uid=" + uid +
                '}';
    }
}
