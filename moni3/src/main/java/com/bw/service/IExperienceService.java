package com.bw.service;

import com.bw.entity.Experience;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
public interface IExperienceService extends IService<Experience> {

}
