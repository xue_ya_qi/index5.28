package com.bw.service.impl;

import com.bw.entity.Resume;
import com.bw.mapper.ResumeMapper;
import com.bw.service.IResumeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
@Service
public class ResumeServiceImpl extends ServiceImpl<ResumeMapper, Resume> implements IResumeService {

}
