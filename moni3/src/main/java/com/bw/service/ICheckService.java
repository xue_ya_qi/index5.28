package com.bw.service;

import com.bw.entity.Check;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
public interface ICheckService extends IService<Check> {

}
