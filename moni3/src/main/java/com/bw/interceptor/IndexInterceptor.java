package com.bw.interceptor;

import com.bw.utils.auth.JwtUtils;
import com.bw.utils.auth.RsaUtils;
import com.bw.utils.auth.UserInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.PublicKey;

@Configuration
public class IndexInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if(StringUtils.isEmpty("token")){
            return false;
        }
        PublicKey publicKey = RsaUtils.getPublicKey("D:\\vueSOFT\\jwt\\key\\rsa.pub");
        UserInfo userInfo = JwtUtils.getInfoFromToken(token, publicKey);
        if(userInfo!=null){
            return true;
        }
        return false;
    }
}
