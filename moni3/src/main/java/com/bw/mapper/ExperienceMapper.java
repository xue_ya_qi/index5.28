package com.bw.mapper;

import com.bw.entity.Experience;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
public interface ExperienceMapper extends BaseMapper<Experience> {

}
