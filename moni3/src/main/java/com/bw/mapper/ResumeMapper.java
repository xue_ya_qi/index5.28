package com.bw.mapper;

import com.bw.entity.Resume;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
public interface ResumeMapper extends BaseMapper<Resume> {

}
