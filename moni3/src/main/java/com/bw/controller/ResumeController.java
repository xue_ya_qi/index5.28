package com.bw.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.entity.Doubles;
import com.bw.entity.Experience;
import com.bw.entity.Resume;
import com.bw.service.IExperienceService;
import com.bw.service.IResumeService;
import com.bw.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/bw/resume")
public class ResumeController {
    @Autowired
    private IResumeService iResumeService;
    @Autowired
    private IExperienceService iExperienceService;
    @PostMapping("list")
    public ResponseResult list(@RequestBody Resume resume){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper wrapper = new QueryWrapper();
            if(resume.getStatus()!=null){
                wrapper.eq("status",resume.getStatus());
            }
            List list = iResumeService.list(wrapper);
            result.setResult(list);
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("查询列表失败"+e.toString());
        }
        return result;
    }
    @GetMapping("/updatestatus/{rid}")
    public ResponseResult updatestatus(@PathVariable int rid){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            Resume resume = iResumeService.getById(rid);
            if(resume.getStatus()==0){
                resume.setStatus(1);
            }
            iResumeService.saveOrUpdate(resume);
            result.setMessage("修改成功");
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("修改失败"+e.toString());
        }
        return result;
    }
    @PostMapping("/add")
    public ResponseResult add(@RequestBody Doubles doubles){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            Resume resume=doubles.getResume();
            resume.setStatus(0);
            iResumeService.saveOrUpdate(resume);
            List<Experience> experiences=doubles.getExperiences();
            for (Experience e:experiences) {
                e.setRid(resume.getRid());
                iExperienceService.saveOrUpdate(e);
            }
            result.setMessage("添加成功");
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("修改失败"+e.toString());
        }
        return result;
    }
    @PostMapping("/adminlist")
    public ResponseResult adminlist(@RequestBody Resume resume){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.ne("status",0);
            if(resume.getPname()!=null){
                wrapper.like("pname",resume.getPname());
            }
            List list = iResumeService.list(wrapper);
            result.setResult(list);
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("查询列表失败"+e.toString());
        }
        return result;
    }
    @GetMapping("/hx/{rid}")
    public ResponseResult hx(@PathVariable int rid){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            Resume resume = iResumeService.getById(rid);
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("rid",rid);
            List<Experience> experiences = iExperienceService.list(wrapper);
            Doubles doubles = new Doubles();
            doubles.setExperiences(experiences);
            doubles.setResume(resume);
            result.setResult(doubles);
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("回显失败"+e.toString());
        }
        return result;
    }
}
