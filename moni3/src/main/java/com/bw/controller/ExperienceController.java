package com.bw.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.entity.Experience;
import com.bw.service.IExperienceService;
import com.bw.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/bw/experience")
public class ExperienceController {
    @Autowired
    private IExperienceService iExperienceService;
    @GetMapping("/list/{rid}")
    public ResponseResult list(@PathVariable int rid){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("rid",rid);
            List list = iExperienceService.list(wrapper);
            result.setResult(list);
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("查询经历失败"+e.toString());
        }
        return result;
    }
}
