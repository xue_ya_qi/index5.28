package com.bw.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.entity.User;
import com.bw.service.IUserService;
import com.bw.utils.ResponseResult;
import com.bw.utils.auth.JwtUtils;
import com.bw.utils.auth.RsaUtils;
import com.bw.utils.auth.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.PrivateKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/bw/user")
public class UserController {
    @Autowired
    private IUserService iUserService;

    @PostMapping("/login")
    public ResponseResult login(@RequestBody User user) {
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper wrapper = new QueryWrapper();
            Map map = new HashMap();
            map.put("username",user.getUsername());
            map.put("pwd",user.getPwd());
            wrapper.allEq(map);
            List<User> list = iUserService.list(wrapper);
            if (list.size() > 0) {
                User u = (User) list.get(0);
                result.setMessage("登陆成功,欢迎用户   (" + u.getUname()+")");
                UserInfo userInfo = new UserInfo();
                userInfo.setUsername(u.getUsername());
                userInfo.setId(new Long(u.getUid()));
                try {
                    PrivateKey privateKey = RsaUtils.getPrivateKey("D:\\vueSOFT\\jwt\\key\\rsa.pri");
                    String token = JwtUtils.generateToken(userInfo, privateKey, 30);
                    Map hashMap = new HashMap();
                    hashMap.put("user", u);
                    hashMap.put("token", token);
                    result.setResult(hashMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                result.setMessage("账号密码错误");
                result.setSuccess(false);
            }
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("登录失败"+e.toString());
        }
        return result;
    }
}
