package com.bw.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bw.entity.Check;
import com.bw.entity.Resume;
import com.bw.service.ICheckService;
import com.bw.service.IResumeService;
import com.bw.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizhengqian
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/bw/check")
public class CheckController {
    @Autowired
    private ICheckService iCheckService;
    @Autowired
    private IResumeService iResumeService;
    @PostMapping("/upstatus")
    public ResponseResult upstatus(@RequestBody Check check){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            Resume resume = iResumeService.getById(check.getRid());
            if(resume.getStatus()==1){
                resume.setStatus(2);
            }
            check.setCdate(new Date());
            iCheckService.saveOrUpdate(check);
            iResumeService.saveOrUpdate(resume);
            result.setMessage("操作成功");
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("操作失败"+e.toString());
            System.out.println(e.toString());
        }
        return result;
    }
    @GetMapping("/checkedHx/{rid}")
    public ResponseResult hx(@PathVariable int rid){
        ResponseResult result = ResponseResult.SUCCESS();
        try {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("rid",rid);
            List list = iCheckService.list(wrapper);
            result.setResult(list);
        }catch (Exception e){
            result.setSuccess(false);
            result.setMessage("回显失败"+e.toString());
        }
        return result;
    }
}
