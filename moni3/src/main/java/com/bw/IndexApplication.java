package com.bw;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.bw.mapper")
public class IndexApplication {
    public static void main(String[] args) {
        SpringApplication.run(IndexApplication.class);
    }
}
